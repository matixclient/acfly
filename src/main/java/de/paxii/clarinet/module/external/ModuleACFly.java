package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

/**
 * Created by Lars on 24.03.2016.
 */
public class ModuleACFly extends Module {
  public ModuleACFly() {
    super("ACFly", ModuleCategory.MOVEMENT, -1);

    this.setVersion("1.0");
    this.setBuildVersion(15801);
  }


  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);

  }

  @EventHandler
  public void onTick(IngameTickEvent event) {
    Wrapper.getPlayer().onGround = true;

    if (Wrapper.getGameSettings().keyBindJump.isKeyDown()) {
      Wrapper.getPlayer().motionY += 0.0055;

    }
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);

  }
}
